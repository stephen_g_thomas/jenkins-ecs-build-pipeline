# Jenkins ECS Build Pipeline Template

sBuild

This repository contains a template to quickly spin up a jenkins build pipeline for AWS ECS
  
This build pipeline builds and tests docker on each commit and deploys the images to amazon ECR
  
![Architecture](planning/architecture.png)

## Deployment

Jenkins URL: http://ec2-54-185-220-203.us-west-2.compute.amazonaws.com:8080 
  
Dom login: Dkiller, password
Steve login: Steve, steve12195
  
Bitbucket Integration enabled under "bitbucket settings/webhooks"

